/*
 * Title:        CloudSim Toolkit
 * Description:  CloudSim (Cloud Simulation) Toolkit for Modeling and Simulation
 *               of Clouds
 * Licence:      GPL - http://www.gnu.org/copyleft/gpl.html
 *
 * Copyright (c) 2009, The University of Melbourne, Australia
 */
package org.cloudbus.cloudsim.AutonomicLoadManagementStrategies;

import org.cloudbus.cloudsim.allocationpolicies.VmAllocationPolicy;
import org.cloudbus.cloudsim.allocationpolicies.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.brokers.DatacenterBroker;
import org.cloudbus.cloudsim.brokers.DatacenterBrokerSimple;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.cloudlets.CloudletSimple;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.datacenters.Datacenter;
import org.cloudbus.cloudsim.datacenters.DatacenterSimple;
import org.cloudbus.cloudsim.hosts.Host;
import org.cloudbus.cloudsim.hosts.HostSimple;
import org.cloudbus.cloudsim.power.models.PowerAware;
import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.power.models.PowerModelLinear;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.ResourceProvisionerSimple;
import org.cloudbus.cloudsim.resources.Pe;
import org.cloudbus.cloudsim.resources.PeSimple;
import org.cloudbus.cloudsim.schedulers.cloudlet.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.schedulers.vm.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModel;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModelDynamic;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModelFull;
import org.cloudbus.cloudsim.vms.Vm;
import org.cloudbus.cloudsim.vms.VmSimple;
import org.cloudsimplus.builders.tables.CloudletsTableBuilder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * This class is used to print the simulation results. This class is still under development
 * @see SimulationResults#printHostCpuUtilizationAndPowerConsumption(CloudSim simulation, DatacenterBroker broker, List<Host> hostList)
 *
 */
public class SimulationResults {
    /**
     * Main method
     * @param args
     */	
    public static void main(String[] args) 
    {
        new SimulationResults();
    }
    
    public SimulationResults()
    {
    	
    }
    private boolean showAllHostUtilizationHistoryEntries;
    /**
     * @param simulation is the created instance of the class  {@link CloudSim} through which the simulation engine is launched
     * @param broker is the created data center broker used in the simulation 
     * @param hostList is the created hosts during the simulation 
     * 
     * @author Abdulrahman Nahhas
     * @since CloudSim Plus 1.0
     */	

    public void printHostCpuUtilizationAndPowerConsumption(CloudSim simulation, DatacenterBroker broker, List<Host> hostList) 
    {
    	int SCHEDULING_INTERVAL = ModelConstruction.SCHEDULE_INTERVAL;
        
    	
        
        List<Cloudlet> newList = broker.getCloudletFinishedList();
        new CloudletsTableBuilder(newList).build();
        System.out.println(getClass().getSimpleName() + " finished!");
        
        /**
        Since the utilization history are stored in the reverse chronological order,
        the values are presented in this way.
         */
        
    	System.out.println();
        for (Host host : hostList) {
            System.out.printf("Host %d CPU utilization and power consumption\n", host.getId());
            System.out.println("-------------------------------------------------------------------------------------------");
            double prevUtilizationPercent = -1, prevWattsPerInterval = -1;
            final  Map<Double, DoubleSummaryStatistics> utilizationPercentHistory = host.getUtilizationHistory();
            
            double totalPowerWattsSec = 0;
            double time = simulation.clock();
          //time difference from the current to the previous line in the history
            double utilizationHistoryTimeInterval;
            double prevTime=0;
            for (Map.Entry<Double, DoubleSummaryStatistics> entry : utilizationPercentHistory.entrySet()) {
            	utilizationHistoryTimeInterval = entry.getKey() - prevTime;
            	final double utilizationPercent = entry.getValue().getSum();
                
                /**
                 * The power consumption is returned in Watt-second,
                 * but it's measured the continuous consumption before a given time,
                 * according to the time interval defined by {@link #SCHEDULING_INTERVAL} set to the Datacenter.
                */
            	
            	final double wattsSec = host.getPowerModel().getPower(utilizationPercent);
                final double wattsPerInterval = wattsSec*utilizationHistoryTimeInterval;
                totalPowerWattsSec += wattsPerInterval;
                
                      if(showAllHostUtilizationHistoryEntries || prevUtilizationPercent != utilizationPercent || prevWattsPerInterval != wattsPerInterval) {
                System.out.printf("\tTime %8.2f | CPU Utilization %6.2f%% | Power Consumption: %8.0f Watt-Second in %f Seconds\n",
                        entry.getKey(), utilizationPercent * 100, wattsPerInterval, utilizationHistoryTimeInterval);
                } 
                 //   commented for minimizing console output
                
                prevUtilizationPercent = utilizationPercent;
                prevWattsPerInterval = wattsPerInterval;
                prevTime = entry.getKey();
                
            }
                System.out.printf(
                    "Total Host %d Power Consumption in %.0f secs: %.2f Watt-Sec (mean of %.2f Watt-Second)\n",
                    host.getId(), simulation.clock(), totalPowerWattsSec, totalPowerWattsSec/simulation.clock());
                final double powerWattsSecMean = totalPowerWattsSec / simulation.clock();
                System.out.printf(
                    "Mean %.2f Watt-Sec for %d usage samples (%.5f KWatt-Hour)\n",
                    powerWattsSecMean, utilizationPercentHistory.size(), PowerAware.wattsSecToKWattsHour(powerWattsSecMean));
                 System.out.println("-------------------------------------------------------------------------------------------\n");
            //       commented for minimizing console output
        
        }
    }
}
