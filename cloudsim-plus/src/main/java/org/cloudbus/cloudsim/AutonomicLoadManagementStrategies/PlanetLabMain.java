/**
 * 
 */
package org.cloudbus.cloudsim.AutonomicLoadManagementStrategies;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.allocationpolicies.migration.VmAllocationPolicyMigrationStaticThreshold;
import org.cloudbus.cloudsim.brokers.DatacenterBroker;
import org.cloudbus.cloudsim.brokers.DatacenterBrokerSimple;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.datacenters.DatacenterSimple;
import org.cloudbus.cloudsim.hosts.Host;
import org.cloudbus.cloudsim.vms.Vm;
import org.cloudsimplus.listeners.EventInfo;

/**
 * @author Shyam Sundar V
 *
 */
public class PlanetLabMain {

    public CloudSim simulation;
	public List<Host> hostsListDC1 = new ArrayList<>();
    private int HighcpuMedium=1;
    private int Extralarge=2;
    private int Small=3;
    private int Micro=4;

    double TIME_TO_TERMINATE_SIMULATION_Planetlab=(24*60*60)-600;
    
    public static void main(String[] args) 
    {
        new PlanetLabMain();
    }
	
    public PlanetLabMain() 
    {  
    	System.out.println("Starting " + getClass().getSimpleName());
  	  	simulation = new CloudSim();
    	simulation.terminateAt(TIME_TO_TERMINATE_SIMULATION_Planetlab);
 	     
	    CreateDatacenterDC1 createDC1 = new CreateDatacenterDC1();
	    for(int i=0;i<400;i++)
	    {
		  createDC1.createHostsDC1(1,1);//"HP Proliant G4"); //400
		  createDC1.createHostsDC1(1,2);//"HP Proliant G5");
	    }
	    DatacenterBroker brokerDC1 = new DatacenterBrokerSimple(simulation);
	    DatacenterSimple dc1 = createDC1.creatingSimpleDatacenterDC1(simulation,brokerDC1);
	    
	    HeuristicAlgorithms setPolicyDC1 = new HeuristicAlgorithms();
	    setPolicyDC1.setAllocationPolicy (dc1,1);
	    
	    InitializationDC1 createCloudletandVm =new InitializationDC1();
	    createCloudletandVm.createOneVmAndCloudlet(100, HighcpuMedium, brokerDC1);//100
	    createCloudletandVm.createOneVmAndCloudlet(100, Extralarge, brokerDC1);//100
	    createCloudletandVm.createOneVmAndCloudlet(300, Small, brokerDC1);//300
	    createCloudletandVm.createOneVmAndCloudlet(300, Micro, brokerDC1);//300
  
	    hostsListDC1.addAll(createDC1.getHostsListDC1());

	    simulation.start();
	    SimulationResults printresults = new SimulationResults(); 
	    printresults.printHostCpuUtilizationAndPowerConsumption(simulation, brokerDC1, hostsListDC1);      
    }
    
}
